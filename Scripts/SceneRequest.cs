﻿namespace NMeijer.SceneLoading
{
	public abstract class SceneRequest
	{
		#region Properties

		public abstract string ScenePath { get; }

		public abstract string LoadingScenePath { get; }

		public bool IsFinished { get; private set; }

		public bool SkipFadeIn { get; }

		#endregion

		protected SceneRequest(bool skipFadeIn = false)
		{
			SkipFadeIn = skipFadeIn;
		}

		#region Internal Methods

		/// <summary>
		/// Called by a <see cref="LoadingSceneView"/> to confirm the scene request has finished loading.
		/// </summary>
		internal void Finish()
		{
			IsFinished = true;
		}

		#endregion
	}
}