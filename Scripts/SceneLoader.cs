﻿using UnityEngine.SceneManagement;

namespace NMeijer.SceneLoading
{
	public static class SceneLoader
	{
		#region Properties

		public static SceneRequest CurrentSceneRequest
		{
			get; private set;
		}

		#endregion

		#region Public Methods

		public static void RequestSceneChange(SceneRequest sceneRequest)
		{
			if(CurrentSceneRequest == null || CurrentSceneRequest.IsFinished)
			{
				CurrentSceneRequest = sceneRequest;
				SceneManager.LoadScene(CurrentSceneRequest.LoadingScenePath, LoadSceneMode.Additive);
			}
		}

		public static bool TryGetCurrentSceneRequest<T>(out T sceneRequest)
			where T : SceneRequest
		{
			sceneRequest = CurrentSceneRequest as T;
			return sceneRequest != null;
		}

		#endregion
	}
}